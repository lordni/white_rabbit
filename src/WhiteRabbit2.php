<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        // Harcoded coins array reverse for search purpose
        $coins = array("100", "50", "20","10", "5" , "2", "1");
        $returncoins = array("1" => 0, "2" => 0, "5" => 0, "10" => 0, "20" => 0, "50" => 0, "100" => 0);

        // Run while amount is more the zero
        while($amount > 0) {

            foreach ($coins as $coin) {
                if (!($amount - $coin < 0)) {
                    $amount -= $coin;
                    $returncoins[$coin] += 1;
                    break;
                }
            }
        }

        return $returncoins;
    }
}