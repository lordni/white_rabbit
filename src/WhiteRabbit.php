<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!
        //echo $filePath;
        return strtolower(file_get_contents($filePath));
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $lettercounterArray = array(); // Place holder for all letters

        $counts = array(); // All the counts of the letters used for sort and retreiving the data.
        $lettercount = array(); // Simple placeholder for array attributes
        // Filter to only letters
        foreach (count_chars($parsedFile, 1) as $i => $val) {
            if ($i > 96 && $i < 123) {
                $lettercount["letter"] = chr($i);
                $lettercount["count"] = $val;

                array_push($counts, $val);
                //array_push($lettercount, chr($i), $val);
                array_push($lettercounterArray, $lettercount);
                $lettercount = array();
            }
        }


        $values = array();
        $half = count($counts) / 2;

        $selecter = 0;
        foreach ($counts as $i => $val) {
            //echo 'I: ' . $i . '; value: ' . $val;
            if ($selecter == $half || $selecter == $half - 1) {
                array_push($values, $val);
            }
            $selecter++;
        }

        $returnarray = array();


        foreach ($lettercounterArray as $row) {

            foreach ($values as $val) {
                if ($val == $row["count"]) {
                    $lettercount["letter"] = $row["letter"];
                    $lettercount["count"] = $row["count"];
                    array_push($returnarray, $lettercount);
                }
            }
        }

        $occurrences = $returnarray[0]["count"] && $returnarray[1]["count"];
        return $returnarray[0]["letter"] && $returnarray[1]["letter"];
    }

    private function usedcomments()
    {
        /*
         * //echo print_r($lettercounterArray);
        //echo $allValues . ' : ';
        //echo sizeof($lettercounterArray) * 2  . ' : ';
        //echo $allValues / (sizeof($lettercounterArray) * 2) . ' : ';
        /*
        echo array_sum($trimArray) . ' : ';
        echo sizeof( $trimArray) . ' : ';
        echo array_sum($trimArray) / sizeof($trimArray) . ' : ';

        echo print_r($trimArray);

        echo array_sum($countedArray) / sizeof($countedArray) . ' : ';
        echo print_r($countedArray);
*/
        //echo print_r($letterArray);
        /*if ($temp2 != null) {
            $occurrences = $temp2, $temp1;
            return array($temp2, $temp1);
        } else {
            $occurrences = $temp1["count"];
            return $temp1;
        }
        */
    }
}